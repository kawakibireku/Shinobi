#!/bin/bash
echo "Shinobi Installer"
echo "========"
echo "Select your OS"
echo "If your OS is not on the list please refer to the docs."
echo "========"
echo "1. Ubuntu - Fast and Touchless"
echo "2. Ubuntu - Advanced"
echo "3. CentOS"
echo "4. CentOS - Quick Install"
echo "5. MacOS"
echo "6. FreeBSD"
echo "7. OpenSUSE"
echo "========"
chmod +x INSTALL/ubuntu-touchless.sh
sh INSTALL/ubuntu-touchless.sh
